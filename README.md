# IEEE-abrv-names

You can use the `unsrt2authabbrvpp.bst` to abbreviate author names and and reduce the authors list using "et al.".
I have tested it with only IEEE template but it might work with other templates as well

- Just download the `unsrt2authabbrvpp.bst` file and put it with your other latex files
- Change the bibliography stlye in your main TeX file using the following command

`\bibliographystyle{unsrt2authabbrvpp}`